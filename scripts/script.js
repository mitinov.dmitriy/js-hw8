window.addEventListener("load", function () {

    function validateInput(value) {
        return !(Number(value) < 0 || isNaN(Number(value)))
    }

    function showRemoveErrorMessage(domElemToAttach, errorMessage, action = "show") {
        if (action === "show") {
            const errorBlock = document.createElement("span");
            for (const elem of domElemToAttach.children) {
                if (elem.className === "message_error") {
                    elem.remove();
                }
            }
            errorBlock.innerHTML = errorMessage;
            errorBlock.classList.add("message_error");
            domElemToAttach.append(errorBlock);
        }
        if (action === "remove") {
            for (const elem of domElemToAttach.children) {
                if (elem.className === "message_error") {
                    elem.remove();
                }
            }
        }
    }

    function removePriceTag(domElemToCheck) {
        for (const elem of domElemToCheck.children) {
            if (elem.className === "price_tag") {
                elem.remove();
            }
        }
    }

    function highlightInputField() {
        const containerBlock = document.querySelector(".container");
        this.classList.remove("invalid_field");
        this.classList.add("field_focused");
        removePriceTag(containerBlock);
        showRemoveErrorMessage(containerBlock, "", "remove")
    }

    function readAndShowPrice() {
        this.classList.remove("field_focused");
        const containerBlock = document.querySelector(".container");
        if (!validateInput(this.value)) {
            removePriceTag(containerBlock);
            this.classList.add("invalid_field");
            showRemoveErrorMessage(containerBlock, "Please enter correct price")
            this.classList.remove("price_shown")
        } else {
            if (!!this.value){
                removePriceTag(containerBlock);
                const priceTag = document.createElement("span");
                const priceTagCloseBtn = document.createElement("button");
                priceTag.classList.add("price_tag");
                priceTag.innerText = `Текущая цена: ${this.value}`;
                priceTagCloseBtn.classList.add("close_btn");
                priceTagCloseBtn.innerText = "X";
                priceTag.append(priceTagCloseBtn);
                containerBlock.prepend(priceTag);
                this.classList.add("price_shown")
                priceTagCloseBtn.addEventListener("click", function () {
                    priceTag.remove();
                    this.remove();
                    priceField.value = "";

                })
            }
        }
    }


    let priceField = document.getElementById("priceField");
    priceField.addEventListener("focus", highlightInputField)
    priceField.addEventListener("blur", readAndShowPrice)
});
